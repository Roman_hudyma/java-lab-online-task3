package org.epam.exeptions;

import org.epam.exeptions.config.FileCloseExcepition;
import org.epam.exeptions.config.FileNotFoundException;
import org.epam.exeptions.config.FileReader;

public class Main {
    public static void main(String[] args) {


        FileReader reader = new FileReader();
        try {
            reader.read("Hello.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}