package org.epam.exeptions.config;

public class FileReader implements AutoCloseable {
    public void read(String path)throws FileNotFoundException{
        throw new FileNotFoundException("File not found, check the path!");

    }
    @Override
    public void close() throws FileCloseExcepition {
throw new FileCloseExcepition("Something goes wrong when closing file!");
    }

}
