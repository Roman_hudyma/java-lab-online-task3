package org.epam.exeptions.config;

public class FileCloseExcepition extends Exception {
    public FileCloseExcepition() {
        super();
    }
    public FileCloseExcepition(String message) {
        super(message);
    }

    public FileCloseExcepition(String message, Throwable cause) {
        super(message, cause);
    }

    public FileCloseExcepition(Throwable cause) {
        super(cause);
    }
}
