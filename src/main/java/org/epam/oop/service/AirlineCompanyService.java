package org.epam.oop.service;

import org.epam.oop.airplane.Plane;
import java.util.List;
import java.util.stream.DoubleStream;

public interface AirlineCompanyService {
    int sumUploadCapacity();
    int sumCarryingCapacity();
    List<Plane> sortByFlightRangel();
    DoubleStream findByFuelConsurmation();
}
