package org.epam.oop.service;

public class FunctionNotFoundExeption extends Exception {
    public FunctionNotFoundExeption() {
    }

    public FunctionNotFoundExeption(String message) {
        super(message);
    }

    public FunctionNotFoundExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public FunctionNotFoundExeption(Throwable cause) {
        super(cause);
    }

    public FunctionNotFoundExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}



