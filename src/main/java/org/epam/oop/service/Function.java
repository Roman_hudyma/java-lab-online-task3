package org.epam.oop.service;

public enum Function {
    SHOW("Show all planes sorted by flight range"),
    SUM("Show sum and show upload capacity and carrying capacity "),
    FURTHER("Show the plane with the largest range of fly"),
    QUIT("End the program");

    private String clarification;

    Function(String clarification) {
        this.clarification = clarification;
    }

    public String getClarification() {
        return clarification;
    }

    public void setClarification(String clarification) {
        this.clarification = clarification;
    }
}
