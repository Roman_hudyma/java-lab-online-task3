package org.epam.oop.service;

import org.epam.oop.airplane.Plane;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class AirlineCompany implements AirlineCompanyService {
    protected List<Plane> airport;


    @Override
    public int sumUploadCapacity() {
        return (int) airport.stream()
                .mapToDouble(Plane::getUploadCapacity)
                .sum();
    }

    @Override
    public int sumCarryingCapacity() {
        return (int) airport.stream()
                .mapToDouble(Plane::getCarryingCapacity)
                .sum();
    }

    @Override
    public List<Plane> sortByFlightRangel() {
        return airport.stream()
                .sorted(Comparator.comparing(Plane::getFlightRange))
                .collect(Collectors.toList());
    }
    public DoubleStream findByFuelConsurmation()
    {
        return airport.stream()
                .mapToDouble(Plane::getFuelConsumption);
    }


}
