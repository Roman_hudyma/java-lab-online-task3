package org.epam.oop.airplane;

public class PlaneNotFoundExceptions  extends Exception{
    public PlaneNotFoundExceptions() {
        super();
    }

    public PlaneNotFoundExceptions(String message) {
        super(message);
    }

    public PlaneNotFoundExceptions(String message, Throwable cause) {
        super(message, cause);
    }

    public PlaneNotFoundExceptions(Throwable cause) {
        super(cause);
    }

    protected PlaneNotFoundExceptions(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}


