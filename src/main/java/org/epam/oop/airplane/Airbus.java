package org.epam.oop.airplane;

public class Airbus extends Plane {

    public Airbus(String model, int uploadCapacity, int flightRange, int fuelConsumption, int carryingCapacity) {
        super(model, uploadCapacity, flightRange, fuelConsumption, carryingCapacity);
    }
}

