package org.epam.oop.airplane;

public class Basler extends Plane {

    public Basler(String model, int uploadCapacity, int flightRange, int fuelConsumption, int carryingCapacity) {
        super(model, uploadCapacity, flightRange, fuelConsumption, carryingCapacity);
    }
}

