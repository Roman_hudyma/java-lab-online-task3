package org.epam.oop.company;

import org.epam.oop.airplane.*;
import org.epam.oop.service.AirlineCompany;

import java.util.Arrays;
import java.util.List;

public abstract class KinexLogistics extends AirlineCompany {
    private static AirlineCompany kinexLogistics;

    {
        airport = Arrays.asList(
                (new Boeing("Boeing 747 LCF Dreamlifter",40 , 2100, 100, 3000)),
                (new Boeing("Boeing 747 LCF Dreamlifter",70, 1700, 130, 5000)),
                (new Basler("Basler BT-67", 30, 1150, 70, 1100)),
                (new Airbus("Airbus Beluga XL", 50, 1875, 90, 3700))
        );
    }

    public static KinexLogistics getInstance()
    {

            if(kinexLogistics==null)
            {
                return new KinexLogistics() {
                    public int sumcarryingCapacity() {
                        return 0;
                    }

                    @Override
                    public List<Plane> sortByFlightRangel() {
                        return null;
                    }

                };
            }
            return (KinexLogistics) kinexLogistics;

        }
    }

